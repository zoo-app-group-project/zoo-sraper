<?php

declare(strict_types=1);

namespace Zoo\Crawler;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ZooListGermany
 * @package Zoo\Crawler
 */
class ZooListGermany extends ZooList
{
    /**
     * ZooListGermany constructor.
     */
    public function __construct()
    {
        parent::__construct('https://en.wikipedia.org/wiki/List_of_zoos_in_Germany');
    }

    /**
     * @return array
     */
    public function scrap(): array
    {
        $this->crawler->filter('tr>td:nth-child(2)')->each(function (Crawler $node) {

            $zoo = $node->filter('a:first-of-type:not(.new)')->each(function (Crawler $node) {
                return $this->getZoo($node->attr('href'));
            });

            $this->addZoo($zoo);
        });

        return $this->getZoos();
    }
}