<?php

declare(strict_types=1);

namespace Zoo\Crawler;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ZooArticle
 * @package Zoo\Crawler
 */
class ZooArticle extends Scraper
{
    /** @var string */
    protected $articleUrl;

    /**
     * ZooArticle constructor.
     * @param string $articleUrl
     */
    public function __construct(string $articleUrl)
    {
        if (filter_var($articleUrl, FILTER_VALIDATE_URL)) {
            $this->articleUrl = $articleUrl;
        } else {
            $this->articleUrl = 'https://en.wikipedia.org' . $articleUrl;
        }

        parent::__construct($this->articleUrl);
    }

    /**
     * @return array
     */
    public function scrap(): array
    {
        $this->without('.mw-empty-elt');

        $location = $this->getLocation();

        if (empty($location[0])) {
            return [];
        }

        $location = $location[0];

        return [
            'name' => $this->getName(),
            'location' => $location,
            'payload' => [
                'description' => $this->getDescription(),
                'logo' => $this->getLogo(),
                'wikipediaLink' => $this->articleUrl,
                'address' => $this->getAddress($location),
            ]
        ];
    }

    /**
     * @return string
     */
    protected function getName(): string
    {
        return $this->crawler->filter('#firstHeading')->each(function (Crawler $node) {
            return $node->text();
        })[0];
    }

    /**
     * @return array
     */
    protected function getLocation(): array
    {
        return $this->crawler->filter('#coordinates')->each(function (Crawler $node) {
            return $this->parseCoordinates($node->text());
        });
    }

    /**
     * @return string
     */
    protected function getDescription(): string
    {
        return $this->crawler->filter('p:first-of-type')->each(function (Crawler $node) {
            return $this->removeLinkSquareBrackets($node->text());
        })[0];
    }

    /**
     * @return string
     */
    protected function getLogo(): string
    {
        return $this->crawler->filter('img:first-of-type')->each(function (Crawler $node) {

            $alt = $node->attr('alt');

            if (!$alt) {
                return '';
            }

            if ($this->isImgLogo($alt)) {
                return $node->attr('src');
            }

            return '';
        })[0];
    }

    /**
     * @param string $string
     * @return array
     */
    protected function parseCoordinates(string $string): array
    {
        if (empty($string)) {
            return [];
        }

        $string = explode('/', $string)[2];
        $coordinates = explode(';', $string);

        foreach ($coordinates as &$coordinate) {
            $coordinate = $this->removeLinkSquareBrackets($coordinate);
            $coordinate = $this->removeFromText('(Ocean Park)', $coordinate);
            $coordinate = $this->removeFromText('(Johannesburg Zoo)', $coordinate);
            $coordinate = $this->removeFromText('(ZSLWhipsnadeZoo)', $coordinate);
            $coordinate = $this->removeFromText(' ', $coordinate);
        }

        return [
            'latitude' => (float)$coordinates[0],
            'longitude' => (float)$coordinates[1],
        ];
    }

    protected function getAddress(array $coordinates): array
    {
        $geolocalizer = new GeolocalizerClient();
        return $geolocalizer->getAddress($coordinates['latitude'], $coordinates['longitude']);
    }

    /**
     * @param string $alt
     * @return bool
     */
    protected function isImgLogo(string $alt): bool
    {
        return strpos($alt, 'logo') || strpos($alt, 'Logo') !== false;
    }
}