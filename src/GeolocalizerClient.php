<?php

declare(strict_types=1);

namespace Zoo\Crawler;

use GuzzleHttp\Client;

/**
 * Class GeolocalizerClient
 * @package Zoo\Crawler
 */
class GeolocalizerClient
{
    /** @var Client */
    protected $client;

    /**
     * GeolocalizerClient constructor.
     */
    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @param float $latitude
     * @param float $longitude
     * @return array
     */
    public function getAddress(float $latitude, float $longitude): array
    {
        $response = $this->client->get(getenv('GEOLOCALIZER_HOST') . '/geocoding/reverse', [
            'json' => [
                'latitude' => $latitude,
                'longitude' => $longitude,
            ],
            'headers' => $this->getHeaders(),
        ]);

        $response = json_decode($response->getBody()->getContents(), true);

        if (!isset($response['body']['results'][0])) {
            return [];
        }

        $response = $response['body']['results'][0];

        $address = [];

        $address['full'] = $response['name'];

        $address = array_merge($address, $response['address']);

        sleep(1);

        return $address;
    }

    /**
     * @param array $zoo
     */
    public function uploadZoo(array $zoo): void
    {
        $response = $this->client->post(getenv('GEOLOCALIZER_HOST') .  '/points/zoo', [
            'json' => $zoo,
            'headers' => $this->getHeaders()
        ]);
    }

    /**
     * @return array
     */
    protected function getHeaders(): array
    {
        return [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
            'Authorization' => 'Bearer ' . getenv('GEOLOCALIZER_ACCESS_TOKEN'),
        ];
    }
}