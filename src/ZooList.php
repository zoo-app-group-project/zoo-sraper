<?php

declare(strict_types=1);

namespace Zoo\Crawler;

use Symfony\Component\DomCrawler\Crawler;

/**
 * Class ZooList
 * @package Zoo\Crawler
 */
class ZooList extends Scraper
{
    /**
     * @var array
     */
    protected $zoos = [];

    /**
     * ZooList constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        parent::__construct($url);

        $this->without('.tocright')
            ->without('h2')
            ->without('h3')
            ->without('.reference')
            ->without('.noprint')
            ->without('.external')
            ->without('#References')
            ->without('.navbox')
            ->without('.vertical-navbox')
            ->without('#toc')
            ->without('.hlist');
    }

    /**
     * @return array
     */
    public function scrap(): array
    {
        $this->crawler->filter('.mw-parser-output ul>li')->each(function (Crawler $node) {

            $zoo = $node->filter('a:first-of-type:not(.new)')->each(function (Crawler $node) {
                return $this->getZoo($node->attr('href'));
            });

            $this->addZoo($zoo);
        });

        return $this->getZoos();
    }

    /**
     * @param array $zoo
     * @return $this
     */
    protected function addZoo(array $zoo): self
    {
        if (!empty($zoo[0])) {
            $zoo = $zoo[0];
            $this->zoos[] = $zoo;
            echo "Scrapped " . $zoo['name'] . "\n";
        }

        return $this;
    }

    /**
     * @param string $articleUrl
     * @return array
     */
    protected function getZoo(string $articleUrl): array
    {
        $article = new ZooArticle($articleUrl);

        $zoo = $article->scrap();

        if (empty($zoo)) {
            return [];
        }

        return $zoo;
    }

    /**
     * @return array
     */
    protected function getZoos(): array
    {
        return array_values(
            array_filter($this->zoos)
        );
    }
}