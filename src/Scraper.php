<?php

declare(strict_types=1);

namespace Zoo\Crawler;

use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

/**
 * Class Scraper
 * @package Zoo\Crawler
 */
abstract class Scraper
{
    /** @var Crawler */
    protected $crawler;

    /**
     * ZooCrawler constructor.
     * @param string $url
     */
    public function __construct(string $url)
    {
        $client = new Client();
        $this->crawler = $client->request('GET', $url);
    }

    /**
     * @param string $selector
     * @return $this
     */
    public function without(string $selector): self
    {
        $this->crawler->filter($selector)->each(function (Crawler $crawler) {
            foreach ($crawler as $node) {
                $node->parentNode->removeChild($node);
            }
        });

        return $this;
    }

    /**
     * @param string $description
     * @return string
     */
    public function removeLinkSquareBrackets(string $description): string
    {
        for($i = 0; $i < 100; $i++) {
            $description = $this->removeFromText("[$i]", $description);
        }

        return $description;
    }

    /**
     * @param string $toBeRemoved
     * @param string $text
     * @return string
     */
    public function removeFromText(string $toBeRemoved, string $text): string
    {
        return str_replace($toBeRemoved, '', $text);
    }

    /**
     * @return array
     */
    abstract public function scrap(): array;
}