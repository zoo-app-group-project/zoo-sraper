<?php

require ('app.php');

use Zoo\Crawler\GeolocalizerClient;

$client = new GeolocalizerClient();

$zoos = json_decode(file_get_contents('output.json'), true);

foreach ($zoos as $zoo) {
    $client->uploadZoo($zoo);

    echo 'Uploaded ' . $zoo['name'] . "\n";
}





