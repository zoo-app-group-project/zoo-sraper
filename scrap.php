<?php

require ('app.php');

use Zoo\Crawler\ZooList;
use Zoo\Crawler\ZooListGermany;

$articles = [
    'https://en.wikipedia.org/wiki/List_of_zoos_in_Pakistan',
    'https://en.wikipedia.org/wiki/List_of_zoos_in_Bangladesh',
    'https://en.wikipedia.org/wiki/List_of_zoos_in_India',
    'https://en.wikipedia.org/wiki/List_of_zoos_in_Japan',
    'https://en.wikipedia.org/wiki/List_of_zoos_in_the_United_States',
    'https://en.wikipedia.org/wiki/List_of_zoos_in_Australia',
    'https://en.wikipedia.org/wiki/List_of_zoos_in_Canada',
    'https://en.wikipedia.org/wiki/List_of_zoos_by_country',
];

$results = [];

foreach ($articles as $article) {
    /** @var ZooList $scraper */
    $scraper = new ZooList($article);
    $zoos = $scraper->scrap();
    $results = array_merge($results, $zoos);
}

$scraper = new ZooListGermany();
$germanyZoos = $scraper->scrap();

$results = array_merge($results, $germanyZoos);

file_put_contents ("output.json", json_encode($results));

